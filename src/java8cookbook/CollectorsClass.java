/*
 * The MIT License
 *
 * Copyright 2015 mjabadilla.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package java8cookbook;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java8cookbook.bean.PersonBean;

/**
 *
 * @author mjabadilla
 */
public class CollectorsClass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        List<PersonBean> air2 = PersonBean.createAir2();
        air2.addAll(PersonBean.createCra3());
        
        Map<Long, List<PersonBean>> collect = air2.stream().collect(Collectors.groupingBy(e -> e.getId()));
        
        System.out.println(collect);
        
        List<String> cra2 = Util.createCra2();
        String joining = cra2.stream().collect(Collectors.joining("|"));
        System.out.println(joining);
        System.out.println(air2.stream().collect(Collectors.partitioningBy(e -> e.getName().length() >= 4)));
    }
    
}
