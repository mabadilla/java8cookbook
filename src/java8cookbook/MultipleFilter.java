/*
 * The MIT License
 *
 * Copyright 2015 mjabadilla.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package java8cookbook;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.function.Predicate;

/**
 *
 * @author mjabadilla
 */
public class MultipleFilter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Collection<String> withNullElement = new ArrayList<>();

        withNullElement.add(null);
        withNullElement.add("notNull");

        withNullElement.stream().filter(Objects::nonNull).filter(e -> e.length() > 0).forEach(System.out::println);
        
        //alternative way
        System.out.println("Alternative");
        Predicate<String> notNullObj = Objects::nonNull;
        Predicate<String> notEmptyLength = e -> e.length() > 0;
        
        Predicate<String> fullPredicate = notNullObj.and(notEmptyLength);
        withNullElement.stream().filter(fullPredicate).forEach(System.out::println);
    }

}
