/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8cookbook;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mjabadilla
 */
public abstract class Util {

    public static Map<String, List> createTeams() {
        Map<String, List> teams = new HashMap<>();
        teams.put("air1", createAir1());
        teams.put("air2", createAir2());
        teams.put("cra2", createCra2());
        teams.put("cra3", createCra3());
        return teams;
    }

    public static List<String> createCra2() {
        List<String> cra2 = new ArrayList<>();
        cra2.addAll(Arrays.asList("Luwi", "Thea", "Kit", "Donard", "Tewen"));
        return cra2;
    }

    public static List<String> createCra3() {
        List<String> cra3 = new ArrayList<>();
        cra3.addAll(Arrays.asList("Juan", "Mark", "Alx", "Jc", "Ed"));
        return cra3;
    }

    public static List<String> createAir2() {
        List<String> air2 = new ArrayList<>();
        air2.addAll(Arrays.asList("Mike", "Shelley", "Jim", "Pau", "Juriel"));
        return air2;
    }

    public static List<String> createAir1() {
        List<String> air1 = new ArrayList<>();
        air1.addAll(Arrays.asList("Gab", "Marco", "Rona", "Kat", "Glen"));
        return air1;
    }
    
}
