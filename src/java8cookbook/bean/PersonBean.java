/*
 * The MIT License
 *
 * Copyright 2015 mjabadilla.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package java8cookbook.bean;

import java.util.ArrayList;
import java.util.List;

public class PersonBean {

    public PersonBean(long id, String name) {
        this.id = id;
        this.name = name;
    }

    private long id;
    private String name;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PersonBean other = (PersonBean) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "PersonBean{" + "id=" + id + ", name=" + name + '}';
    }

    public static PersonBean createPerson(long id, String name) {
        return new PersonBean(id, name);
    }

    public static List<PersonBean> createAir2() {
        List<PersonBean> team = new ArrayList<>();
        team.add(PersonBean.createPerson(2, "Shelley"));
        team.add(PersonBean.createPerson(5, "Juriel"));
        team.add(PersonBean.createPerson(4, "Jim"));
        team.add(PersonBean.createPerson(1, "Brian"));
        team.add(PersonBean.createPerson(3, "Mike"));
        return team;
    }
    
    public static List<PersonBean> createCra3() {
        List<PersonBean> team = new ArrayList<>();
        team.add(PersonBean.createPerson(2, "Gab"));
        team.add(PersonBean.createPerson(5, "Marco"));
        team.add(PersonBean.createPerson(4, "Rona"));
        team.add(PersonBean.createPerson(1, "Arman"));
        team.add(PersonBean.createPerson(3, "Glenn"));
        team.add(PersonBean.createPerson(6, "Kat"));
        return team;
    }

}
